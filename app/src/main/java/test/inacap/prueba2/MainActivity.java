package test.inacap.prueba2;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import test.inacap.prueba2.Vista.DatosContactoFragment;
import test.inacap.prueba2.Vista.DatosPersonalesFragment;
import test.inacap.prueba2.Vista.InicioSesionFragment;

public class MainActivity extends AppCompatActivity implements
        InicioSesionFragment.OnFragmentInteractionListener,
        DatosContactoFragment.OnFragmentInteractionListener,
        DatosPersonalesFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        DatosPersonalesFragment datosPersonalesFragment = new DatosPersonalesFragment();

        transaction.replace(R.id.f1Contenedor, datosPersonalesFragment).commit();

    }

    @Override
    public void onFragmentInteraction(String nombreFragmento, String evento) {

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        if(nombreFragmento.equals("DatosPersonalesFragment")){
            if(evento.equals("BTN_SIGUIENTE_CLICK")){
                DatosContactoFragment fragment = new DatosContactoFragment();
                transaction.replace(R.id.f1Contenedor,fragment).commit();
            }
        }

        if(nombreFragmento.equals("DatosContactoFragment")){
            if(evento.equals("BTN_SIGUIENTE_CLICK")){

                InicioSesionFragment fragment = new InicioSesionFragment();
                transaction.replace(R.id.f1Contenedor,fragment).commit();
            }
        }

        if(nombreFragmento.equals("DatosInicioSesionFragment")){
            if(evento.equals("BTN_SIGUIENTE_CLICK")){

            }
        }

    }


}
