package test.inacap.prueba2.Vista;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import test.inacap.prueba2.R;

public class DatosContactoFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    public DatosContactoFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.fragment_datos_contacto, container, false);

        final EditText etCorreo = (EditText) layout.findViewById(R.id.etCorreo);
        final EditText etTelefono = (EditText) layout.findViewById(R.id.etTelefono);
        final Button btSiguiente = (Button) layout.findViewById(R.id.btSiguienteDC);

        btSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String correo = etCorreo.getText().toString().trim();

                if (etCorreo.getText().toString().trim().isEmpty())
                    etCorreo.setError("Campo Vacio");

                else if (correo.length() != 0){
                    mListener.onFragmentInteraction("DatosContactoFragment","BTN_SIGUIENTE_CLICK");
                }
            }
        });

        return layout;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction("","");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(String nombreFragmento, String evento);
    }
}